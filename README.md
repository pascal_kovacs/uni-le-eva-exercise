
# EVA-Repository für Übung und Seminar
Dieses Repository dient der Verwaltung und Verteilung von Übungs- und Seminaraufgaben.

Das Repository kannst du mit diesem Befehl herunterladen:
```
git clone https://bitbucket.org/pascal_kovacs/uni-le-eva-exercise
```

Die einzelnen Übungsstände kannst du mit folgendem Befehl auschecken:
```
git checkout tags/<Tag für die Übung>
```

## Git-Installation
Wenn du Git auf deinem persönlichen Rechner installieren willst, folge einfach diesem Link: 
https://git-scm.com/

Ein gutes Tutorial für Git findest du hier:
https://rogerdudler.github.io/git-guide/index.de.html


